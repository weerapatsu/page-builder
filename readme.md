# Page Builder

This project is private mode on Github, for easier to review, I extract only some part of project where I am responsible.

The system provides modules listing where user can pick and compose them for the page,
each module can be configured like content, color etc.

The system will generate 4 versions of this page as mentioned above.
Example page on production https://finance.rabbit.co.th/ 

** *If you open it on mobile devices, the system will serve mobile version.*

## Overview  

  The page need to generate for 4 versions.

- Responsive page - TH
- Responsive page - EN
- Mobile page - TH
- Mobile page - EN   

Due to this system will have many modules, So I design to build a module based on configuration ( [module config](https://bitbucket.org/weerapatsu/page-builder/src/1b35349a43a606806ffe1b9b1040627241d36a6e/config/pagebuilder.php?at=master&fileviewer=file-view-default) )


 And I have vue components to generate module form from those configuration.  

## Frontend code VueJs
[Vue components](https://bitbucket.org/weerapatsu/page-builder/src/1b35349a43a606806ffe1b9b1040627241d36a6e/resources/assets/js/?at=master)

## Backend code Laravel
- [Controllers](https://bitbucket.org/weerapatsu/page-builder/src/6a9057204046f58dc8d8e2326a52e6abf4690fbd/app/Http/Controllers/PageBuilder/?at=master)
- [Repository](https://bitbucket.org/weerapatsu/page-builder/src/6a9057204046f58dc8d8e2326a52e6abf4690fbd/app/Repository/PageBuilderRepository.php?at=master&fileviewer=file-view-default)
- [Service](https://bitbucket.org/weerapatsu/page-builder/src/6a9057204046f58dc8d8e2326a52e6abf4690fbd/app/Services/PageBuilder/TransformerService.php?at=master&fileviewer=file-view-default)


