<?php

return [
    [
        'name' => 'module_title',
        'type' => 'text-content',
        'value' => [
            'th' => 'Video',
            'en' => 'Video',
        ],
    ],
    [
        'name' => 'heading_color',
        'type' => 'color-picker',
        'value' => '#4d4d4d',
    ],
    [
        'name' => 'youtube_link',
        'type' => 'text',
        'value' => 'https://www.youtube.com/watch?v=48LrR1hUkvw',
        'help' => 'https://www.youtube.com/watch?v=<span class=\'text-danger\'>48LrR1hUkvw</span>',
    ],
];
