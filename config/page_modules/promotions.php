<?php

return [
    [
        'name' => 'module_title',
        'type' => 'text-content',
        'value' => [
            'th' => 'โปรโมชั่น',
            'en' => 'Promotions',
        ],
    ],
    [
        'name' => 'heading_color',
        'type' => 'color-picker',
        'value' => '#4d4d4d',
    ],
    [
        'name' => 'background_color',
        'type' => 'color-picker',
        'value' => '#ffffff',
    ],
    [
        'name' => 'see_more_button_text',
        'type' => 'text-content',
        'value' => [
            'th' => 'ดูโปรโมชั่นทั้งหมด',
            'en' => 'See all promotions',
        ],
    ],
    [
        'name' => 'see_more_link',
        'type' => 'text-content',
    ],
    [
        'name' => 'columns',
        'type' => 'radio',
        'value' => '3',
        'values' => [
            '2' => '2 columns',
            '3' => '3 columns',
        ],
    ],
    [
        'name' => 'promotions',
        'type' => 'collection',
        'amount' => 2,
        'min' => 2,
        'attributes' => [
            [
                'name' => 'logo',
                'type' => 'file',
            ],
            [
                'name' => 'image',
                'type' => 'file',
                'value' => 'https://dummyimage.com/600x400/cccccc/4d4d4d.jpg&text=Promotion+image',
            ],
            [
                'name' => 'title',
                'type' => 'text-content',
                'value' => [
                    'th' => 'บัตรเดียวครอบคลุมทุกความต้องการ',
                    'en' => 'All in one Citi card.',
                ],
            ],
            [
                'name' => 'title_color',
                'type' => 'color-picker',
                'value' => '#4d4d4d',
            ],
            [
                'name' => 'desc',
                'type' => 'textarea-content',
                'value' => [
                    'th' => 'สมัครบัตรเครดิตซิตี้ เลือกรับของรางวัลสุดคุ้ม !',
                    'en' => 'Apply Citi Credit Card with rabbit finance. Choose the reward you want!',
                ],
            ],
            [
                'name' => 'desc_color',
                'type' => 'color-picker',
                'value' => '#4d4d4d',
            ],
            [
                'name' => 'button_link',
                'type' => 'text-content',
                'value' => [
                    'th' => 'https://finance.rabbit.co.th/credit-card/citibank-gifts',
                    'en' => 'https://finance.rabbit.co.th/credit-card/citibank-gifts',
                ],
            ],
            [
                'name' => 'button_text',
                'type' => 'text-content',
                'value' => [
                    'th' => 'ประหยัดเลย!',
                    'en' => 'SAVE NOW!',
                ],
            ],
        ]
    ],
];
