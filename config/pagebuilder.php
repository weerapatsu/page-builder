<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Modules registration
    |--------------------------------------------------------------------------
    |
    | These modules will be appear on module list where user can use them for building the page.
    |
    | 'key' => 'name' // key must be matched with file in config/page_modules/*
    */

    'modules' => [
        'promotions' => 'Promotions',
    ],

    'asset' => [
        'styles' => [
            '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css',
            '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css',
            '//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css',
        ],
        'scripts' => [
            '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js',
            '//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.js',
            '//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js',
        ],
    ],

    'page_setting' => [
        'heading' => [
            'underline_width' => 100,
            'underline_color' => '#3b579b',
        ],
        'button' => [
            'font_color' => '#ffffff',
            'background_color' => '#ff9000',
        ],
        'css' => [
            'desktop' => '',
            'mobile' => '',
        ]
    ],

    'page_meta' => [
        'th' => [],
        'en' => [],
    ],
];
