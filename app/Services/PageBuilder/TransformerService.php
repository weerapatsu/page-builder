<?php

namespace App\Services\PageBuilder;

class TransformerService
{
    /**
     * @param array $input
     * @param bool $debug
     *
     * @return array
     */
    public function transformModuleData(array $input, bool $debug = false) : array
    {
        switch ($input['type']) {
            case 'rf-blog':
                $value = $this->transformRfBlog($input);
                break;
            case 'color-picker':
                $value = $this->transformColorPicker($input);
                break;
            default:
                $value = $this->default($input, $debug);
        }

        return $value;
    }

    /**
     * Transform input type rf-blog
     *
     * @param $input
     *
     * @return array
     */
    protected function transformRfBlog($input) : array
    {
        $value = [
            'title' => $input['title'] ?? null,
            'image' => $input['image'] ?? null,
            'url' => $input['value'] ?? null,
        ];

        return $value;
    }

    /**
     * Transform input type 'color-picker'
     *
     * @param $input
     *
     * @return array
     */
    protected function transformColorPicker($input) : array
    {
        if (isset($input['value']['a']) && $input['value']['a'] < 1) {
            $value[$input['name']] = 'rgba(' . $input['value']['rgba']['r'] . ',' . $input['value']['rgba']['g'] . ','
                . $input['value']['rgba']['b'] . ',' . $input['value']['rgba']['a'] . ')';
        } else {
            $value[$input['name']] = $input['value']['hex'] ?? $input['value'];
        }

        return $value;
    }

    /**
     * Transform input default type
     *
     * @param $input
     * @param $debug
     *
     * @return array
     */
    protected function default($input, $debug) : array
    {
        if (is_array($input['value'])) {
            foreach (['th', 'en'] as $locale) {
                $inputValue = $input['value'][$locale];
                $default    = null;
                if ($debug) {
                    $default = 'CMS ' . $input['name'];
                }

                $content = (empty($inputValue) || $inputValue === '<p><br></p>') ? $default : $inputValue;

                $value[$input['name'] . '_' . $locale] = $content;
            }
        } else {
            $value[$input['name']] = empty($input['value']) ? null : $input['value'];
        }

        return $value;
    }

    /**
     * Generate input by always use new config combine with currently data
     *
     * @param $currentInputs
     * @param $config
     *
     * @return array
     */
    public function mergeInputs($currentInputs, $config) : array
    {
        $t = array_column($currentInputs, 'value', 'name');
        $c = array_column($currentInputs, 'collection', 'name');

        foreach ($config as $key => $cInput) {
            if ($cInput['type'] !== 'collection') {
                $config[$key]['value'] = $t[$cInput['name']] ?? $config[$key]['value'];
            } else {
                $config[$key]['collection'] = $c[$cInput['name']] ?? [];
            }
        }

        return $config;
    }
}
