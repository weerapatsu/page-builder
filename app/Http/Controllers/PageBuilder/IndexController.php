<?php

namespace App\Http\Controllers\PageBuilder;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

class IndexController extends Controller
{
    /**
     * @return View
     */
    public function index() : View
    {
        $pageId = 1;

        return view('page-builder.index', compact('pageId'));
    }
}
