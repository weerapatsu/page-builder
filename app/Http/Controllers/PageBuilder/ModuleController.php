<?php

namespace App\Http\Controllers\PageBuilder;

use App\Http\Controllers\Controller;
use App\Repository\PageBuilderRepository;
use Illuminate\Http\JsonResponse;

class ModuleController extends Controller
{
    protected $pageBuilder;

    public function __construct(
        PageBuilderRepository $pageBuilder
    ) {
        $this->pageBuilder = $pageBuilder;
    }

    /**
     * Get modules listing
     *
     * @return JsonResponse
     */
    public function index() : JsonResponse
    {
        $modules = [];
        foreach (config('pagebuilder.modules') as $key => $value) {
            $modules[] = [
                'template' => $key,
                'name' => $value
            ];
        }

        return response()->json($modules);
    }
}
