<?php

namespace App\Http\Controllers\PageBuilder;

use App\Http\Controllers\Controller;
use App\Repository\Cms\RabbitMain\PageBuilderRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Validators\Cms\PageBuilder\PageModuleValidator;
use App\Repository\Cms\RabbitMain\ImageRepository;

class PageModuleController extends Controller
{
    protected $pageBuilder;
    protected $validator;
    protected $cmsImage;

    public function __construct(
        PageBuilderRepository $pageBuilder,
        PageModuleValidator $validator,
        ImageRepository $cmsImage
    ) {
        $this->pageBuilder = $pageBuilder;
        $this->validator   = $validator;
        $this->cmsImage    = $cmsImage;
    }

    /**
     * Get page's modules by Cms id
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function show(int $id) : JsonResponse
    {
        return response()->json($this->pageBuilder->getPageModules($id));
    }

    /**
     * Store new page module
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function store(Request $request) : JsonResponse
    {
        $params = $request->only([
            'cms_rabbit_id',
            'module_template',
            'order'
        ]);

        $this->validator->validateCreate($params);
        $params['properties']['inputs'] = $this->pageBuilder->generateInputsFromConfig($params['module_template']);

        return response()->json($this->pageBuilder->createPageModule($params));
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function destroy(int $id) : JsonResponse
    {
        return response()->json($this->pageBuilder->deletePageModule($id));
    }

    /**
     * Sorting page's modules list
     *
     * @param int $cmsId
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function sortModules(int $cmsId, Request $request) : JsonResponse
    {
        $params = $request->all();

        return response()->json($this->pageBuilder->sortPageModules($cmsId, $params));
    }

    /**
     * Get input fields depends on module
     *
     * @param int $pageModuleId
     *
     * @return JsonResponse
     */
    public function editModule(int $pageModuleId) : JsonResponse
    {
        $pageModule = $this->pageBuilder->getModuleCmsFieldsWithValues($pageModuleId);

        return response()->json([
            $pageModule['module_template'] => $pageModule['properties']
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateModuleProperty(Request $request) : JsonResponse
    {
        $params = $request->all();

        return response()->json($this->pageBuilder->updatePageModuleProperties($params));
    }
}
