<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CmsRabbit extends Model
{
    protected $table = 'cms_rabbit';

    protected $fillable = [
        'page',
        'template_id',
        'responsive_view_th',
        'responsive_view_en',
        'mobile_view_th',
        'mobile_view_en'
    ];
}
