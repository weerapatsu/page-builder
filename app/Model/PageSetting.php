<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageSetting extends Model
{
    protected $table = 'cr_page_settings';

    protected $casts = [
        'meta' => 'array',
        'setting' => 'array'
    ];

    protected $fillable = [
        'cms_rabbit_id',
        'meta',
        'setting'
    ];
}
