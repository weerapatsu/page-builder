<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PageModule extends Model
{
    protected $table = 'cr_page_modules';

    protected $casts = [
        'properties' => 'array'
    ];

    protected $fillable = [
        'page_id',
        'cms_rabbit_id',
        'module_template',
        'properties',
        'order'
    ];

    /**
     * Get page module properties.
     */
    public function pageModuleProperties()
    {
        return $this->hasMany('App\Models\Cms\PageBuilder\PageModuleProperty');
    }
}
