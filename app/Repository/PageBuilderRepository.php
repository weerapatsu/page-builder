<?php

namespace App\Repository;

use App\Models\CmsRabbit;
use App\Models\PageModule;
use App\Models\PageSetting;
use App\Services\PageBuilder\TransformerService;
use Illuminate\Database\Eloquent\Collection;

class PageBuilderRepository
{
    protected $cmsRabbit;
    protected $pageSetting;
    protected $pageModule;
    protected $pageModuleProperty;
    protected $transformer;

    public function __construct(
        CmsRabbit $cmsRabbit,
        PageSetting $pageSetting,
        PageModule $pageModule,
        TransformerService $transformer
    ) {
        $this->cmsRabbit     = $cmsRabbit;
        $this->pageSetting   = $pageSetting;
        $this->pageModule    = $pageModule;
        $this->transformer   = $transformer;
    }

    /**
     * Get page's modules
     *
     * @param int $pageModuleId
     *
     * @return PageModule
     */
    public function getPageModuleById(int $pageModuleId) : PageModule
    {
        return $this->pageModule->findOrfail($pageModuleId);
    }

    /**
     * Get page's modules
     *
     * @param int $cmsId
     *
     * @return array
     */
    public function getPageModules(int $cmsId) : array
    {
        $pageModules = $this->pageModule->where('cms_rabbit_id', '=', $cmsId)
            ->orderBy('order')
            ->get(['id', 'cms_rabbit_id', 'order', 'module_template', 'properties'])
            ->toArray();

        for ($i = 0; $i < count($pageModules); $i++) {
            foreach (['display_on_responsive', 'display_on_mobile'] as $platform_key) {
                $inputId = array_search(
                    $platform_key,
                    array_column($pageModules[$i]['properties']['inputs'], 'name')
                );
                $pageModules[$i][$platform_key] = $pageModules[$i]['properties']['inputs'][$inputId]['value'];
            }
            unset($pageModules[$i]['properties']);
        }

        return $pageModules;
    }

    /**
     * Create new page module
     *
     * @param array $params
     *
     * @return PageModule
     */
    public function createPageModule(array $params) : PageModule
    {
        return $this->pageModule->create($params);
    }

    /**
     * Delete page module
     *
     * @param int $id
     *
     * @return bool
     */
    public function deletePageModule(int $id) : bool
    {
        return $this->pageModule->destroy($id);
    }

    /**
     * Sorting modules
     *
     * @param int $cmsId
     * @param array $modules
     *
     * @return bool
     */
    public function sortPageModules(int $cmsId, array $modules) : bool
    {
        foreach ($modules as $key => $module) {
            // Sorting modules by key
            $module['order']         = $key;
            $module['cms_rabbit_id'] = $cmsId;

            unset($module['name']);

            $this->pageModule->updateOrCreate(
                ['cms_rabbit_id' => $cmsId, 'id' => $module['id']],
                $module
            );
        }

        return true;
    }

    /**
     * @param int $pageModuleId
     * @param string $viewType responsive, mobile
     *
     * @return string
     */
    public function getModuleViewByPageModuleId(int $pageModuleId, string $viewType) : string
    {
        $pageModule  = $this->getPageModuleById($pageModuleId);
        $pageSetting = $this->getPageSetting($pageModule['cms_rabbit_id']);
        $data        = $this->generateModuleViewData($pageModuleId);

        $viewName = 'page-builder.modules.' . $pageModule['module_template'] . '.' . $viewType;

        if (!view()->exists($viewName) && $viewType === 'mobile') {
            $viewName = 'page-builder.modules.' . $pageModule['module_template'] . '.responsive';
        }

        if (view()->exists($viewName)) {
            $view = view($viewName, compact('data', 'pageSetting'))->render();
        } else {
            $view = $pageModule['module_template'] . 'view not found!!';
        }

        return $data['display_on_' . $viewType] ? $view : '';
    }

    /**
     * @param int $cmsId
     *
     * @return string
     */
    public function getSettingStyleHtml(int $cmsId) : string
    {
        $pageSetting = $this->getPageSetting($cmsId);

        return view('page-builder.setting-style', compact('pageSetting'))->render();
    }

    /**
     * @param int $pageModuleId
     *
     * @return array
     */
    protected function generateModuleViewData(int $pageModuleId) : array
    {
        $pageModule   = $this->getPageModuleById($pageModuleId);
        $configInputs = $this->generateInputsFromConfig($pageModule['module_template']);
        $inputs       = $this->transformer->mergeInputs($pageModule['properties']['inputs'], $configInputs);

        $data = [];
        foreach ($inputs as $key => $input) {
            if ($input['type'] === 'collection') {
                $items = [];
                foreach ($input['collection'] as $mKey => $mInputs) {
                    $item = [];
                    foreach ($mInputs as $sKey => $sInputs) {
                        $result = $this->transformer->transformModuleData($sInputs);
                        $item   = array_merge($item, $result);
                    }
                    $items[$mKey] = $item;
                }
                $value[$input['name']] = $items;
            } else {
                $value = $this->transformer->transformModuleData($input);
            }
            $data = array_merge($data, $value);
        }

        return $data;
    }

    /**
     * @param string $template
     *
     * @return array
     */
    public function generateInputsFromConfig(string $template) : array
    {
        $cmsInputs = array_merge(config('page_modules.default'), config('page_modules.' . $template));

        // Apply default data to inputs cms
        foreach ($cmsInputs as $n => $config) {
            // Transform collection input type to proper array
            if ($config['type'] === 'collection') {
                foreach ($config['attributes'] as $key => $attributeConfig) {
                    $attributeConfig          = $this->setDefaultValues($attributeConfig);
                    $fields[]                 = $attributeConfig;
                }

                for ($i = 1; $i <= $config['amount']; $i++) {
                    $cmsInputs[$n]['collection'][] = $fields;
                }
                $cmsInputs[$n]['attributes'] = $fields;
            } else {
                $cmsInputs[$n] = $this->setDefaultValues($config);
            }
        }

        return $cmsInputs;
    }

    /**
     * @param array $config
     *
     * @return array|null
     */
    public function setDefaultValues(array $config)
    {
        if (in_array($config['type'], ['text-content', 'textarea-content', 'html-editor'])) {
            $config['value'] = [
                'th' => $config['value']['th'] ?? null,
                'en' => $config['value']['en'] ?? null
            ];
        } else {
            $config['value'] = $config['value'] ?? null;
        }

        $config['rules'] = $config['rules'] ?? '';

        return $config;
    }

    /**
     * Get page modules view
     * This will combine all of page's modules in the order
     *
     * @param int $cmsId
     * @param string $viewType responsive, mobile
     * @param string $locale
     *
     * @return string
     */
    public function getPageModulesView(
        int $cmsId,
        string $viewType = 'responsive',
        string $locale = 'th'
    ) : string {
        $pageModules = $this->getPageModules($cmsId);
        app()->setLocale($locale);

        $view = '';
        $view .= $this->getSettingStyleHtml($cmsId);

        foreach ($pageModules as $pageModule) {
            $view .= $this->getModuleViewByPageModuleId($pageModule['id'], $viewType);
        }

        return $view;
    }

    /**
     * Save page view
     *
     * @param int $cmsId
     *
     * @return bool
     */
    public function savePageViewByCmsId(int $cmsId) : bool
    {
        $cmsRabbit = $this->cmsRabbit->find($cmsId);

        $cmsRabbit->responsive_view_th = $this->getPageModulesView($cmsId, 'responsive', 'th');
        $cmsRabbit->responsive_view_en = $this->getPageModulesView($cmsId, 'responsive', 'en');
        $cmsRabbit->mobile_view_th     = $this->getPageModulesView($cmsId, 'mobile', 'th');
        $cmsRabbit->mobile_view_en     = $this->getPageModulesView($cmsId, 'mobile', 'en');

        return $cmsRabbit->save();
    }

    /**
     * Get module Cms input fields with data
     *
     * @param int $pageModuleId
     *
     * @return array $moduleCmsInputs
     */
    public function getModuleCmsFieldsWithValues(int $pageModuleId) : array
    {
        $pageModule = $this->getPageModuleById($pageModuleId);

        return $pageModule->toArray();
    }

    /**
     * Update page module properties
     *
     * @param array $params
     *
     * @return bool
     */
    public function updatePageModuleProperties(array $params) : bool
    {
        $pageModule = $this->pageModule->find($params['page_module_id']);

        $pageModule->properties = [
            'inputs' => $params['properties']
        ];

        return $pageModule->save();
    }

    /**
     * Get page module properties
     *
     * @param int $pageModuleId
     *
     * @return Collection
     */
    public function getPageModuleProperties(int $pageModuleId) : Collection
    {
        return $this->pageModule->where('page_module_id', $pageModuleId);
    }

    /**
     * Update page setting
     *
     * @param array $params
     *
     * @return bool
     */
    public function updatePageSetting(array $params) : bool
    {
        $pageSetting = $this->pageSetting->where('cms_rabbit_id', $params['cms_id'])->first();

        $pageSetting->meta    = $params['meta'];
        $pageSetting->setting = $params['setting'];

        return $pageSetting->save();
    }

    /**
     * Get page setting information
     *
     * @param int $cmsId
     *
     * @return array
     */
    public function getPageSetting(int $cmsId) : array
    {
        $pageSetting = $this->pageSetting->where('cms_rabbit_id', $cmsId)->first();

        if (is_null($pageSetting)) {
            $pageSetting = $this->pageSetting->create(['cms_rabbit_id' => $cmsId])->first();
        }

        $pageSetting['setting'] = array_replace_recursive(config('pagebuilder.page_setting'), $pageSetting['setting'] ?? []);

        return $pageSetting['setting'];
    }
}
