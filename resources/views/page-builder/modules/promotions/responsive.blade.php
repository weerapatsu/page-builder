<section class="promotions-section" style="background-color: {{ $data['background_color'] ?? '#ffffff' }};">
    <div class="container">
        <div class="text-center">
            @include('page-builder.components.section_title', [
                'title' => $data[localize_u('module_title')],
                'heading_color' => $data['heading_color'],
            ])
        </div>

        <div class="promotions-section__items flex">
            @foreach($data['promotions'] as $item)
                @continue(!isset($item['image']))

                @if ((($data['columns'] === '2' && $loop->iteration === 5)
                    || ($data['columns'] === '3' && $loop->iteration === 4))
                    && !isset($data['see_more_link_' . app()->getLocale()]))
                    <div class="extra-items-block non-display">
                        <div class="flex">
                @endif
                <div class="promotions-section__item text-center
                    {{ ($data['columns'] === '2' && $loop->iteration > 4)
                    || ($data['columns'] === '3' && $loop->iteration > 3)
                    ? 'extra-item' : '' }} col-{{ $data['columns'] }}"
                    style="background-image: url('{{ $item['image'] }}');">
                    <div class="promotions-section__item-content text-center flex">
                        <div class="promotions-section__item--top">
                            @if (isset($item['logo']))
                                <div class="promotions-section__logo">
                                    <img src="{{ $item['logo'] }}" alt="Logo">
                                </div>
                            @endif

                            <h3 class="promotions-section__title text-center" style="color: {{ $item['title_color'] ?? '#4d4d4d' }};">
                                {{ $item['title_' . app()->getLocale()] }}
                            </h3>

                            <div class="text-center h5" style="color: {{ $item['desc_color'] ?? '#4d4d4d' }};">
                                {!! $item['desc_' . app()->getLocale()] !!}
                            </div>
                        </div>

                        @if (isset($item['button_link_' . app()->getLocale()]))
                            <div class="promotions-section__item--bottom">
                                <div class="promotions-section__btn text-center">
                                    @include('page-builder.components.button', [
                                        'text' => $item[localize_u('button_text')],
                                        'link' => $item[localize_u('button_link')],
                                        'class' => 'btn-lg',
                                    ])
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
            @if ((($data['columns'] === '2' && count($data['promotions']) > 4)
                || ($data['columns'] === '3' && count($data['promotions']) > 3))
                && !isset($data['see_more_link_' . app()->getLocale()]))
                    </div>
                </div>
            @endif
        </div>

        @if ((($data['columns'] === '2' && count($data['promotions']) > 4)
                || ($data['columns'] === '3' && count($data['promotions']) > 3))
                || isset($data['see_more_link_' . app()->getLocale()]))
            <div class="row" id="promotion-show-more-btn-block">
                <div class="col-sm-12 text-center">
                    @include('page-builder.components.button', [
                        'text' => $data[localize_u('see_more_button_text')] ?? '',
                        'link' => $data[localize_u('see_more_link')] ?? '#show-more-btn-block',
                        'class' => 'btn-lg promotions-section__see-more-btn ' . (!isset($data[localize_u('see_more_link')]) ? 'no-link' : ''),
                    ])
                </div>
            </div>
        @endif
    </div>
</section>

<script>
    $(document).ready(function () {
        $('.promotions-section__see-more-btn.no-link').on('click', function (e) {
            if ($('.promotions-section__see-more-btn').hasClass('no-link')) {
                e.preventDefault();
            }

            var $extraItemsBlock = $('.extra-items-block');

            if ($extraItemsBlock.hasClass('non-display')) {
                $extraItemsBlock.slideDown();
                $extraItemsBlock.removeClass('non-display');
            } else {
                $extraItemsBlock.slideUp(function () {
                    $(this).addClass('non-display');
                });
            }
        });
    });
</script>
