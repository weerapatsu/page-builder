<section class="promotions-section">
    <div class="container">
        <div class="text-center">
            @include('page-builder.components.section_title', [
                'title' => $data[localize_u('module_title')],
                'heading_color' => $data['heading_color'],
            ])
        </div>

        <div class="row">
            @foreach($data['promotions'] as $item)
                @continue(!isset($item['image']))
                @if ($loop->iteration === 4 && !isset($data['see_more_link_' . app()->getLocale()]))
                    <div class="extra-items-block non-display">
                @endif
                <div class="col-xs-12 promotions-section__item text-center {{ $loop->iteration > 3 ? 'extra-item' : '' }}"
                    style="background-image: url('{{ $item['image'] }}');">
                    <div class="promotions-section__item-content text-center flex">
                        <div class="promotions-section__item--top">
                            @if (isset($item['logo']))
                                <div class="promotions-section__logo">
                                    <img src="{{ $item['logo'] }}" alt="Logo">
                                </div>
                            @endif

                            <h4 class="promotions-section__title text-center" style="color: {{ $item['title_color'] ?? '#4d4d4d' }};">
                                {{ $item['title_' . app()->getLocale()] }}
                            </h4>

                            <div class="text-center h6" style="color: {{ $item['desc_color'] ?? '#4d4d4d' }};">
                                {!! $item['desc_' . app()->getLocale()] !!}
                            </div>
                        </div>

                        @if (isset($item['button_link_' . app()->getLocale()]))
                            <div class="promotions-section__item--bottom">
                                <div class="promotions-section__btn text-center">
                                    @include('page-builder.components.button', [
                                        'text' => $item[localize_u('button_text')],
                                        'link' => $item[localize_u('button_link')],
                                        'class' => 'btn-lg',
                                    ])
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach
            @if (count($data['promotions']) > 3 && !isset($data['see_more_link_' . app()->getLocale()]))
                </div>
            @endif
        </div>

        @if (count($data['promotions']) > 3 || isset($data['see_more_link_' . app()->getLocale()]))
            <div class="row" id="promotion-show-more-btn-block">
                <div class="col-xs-12 text-center">
                    @include('page-builder.components.button', [
                        'text' => $data[localize_u('see_more_button_text')] ?? '',
                        'link' => $data[localize_u('see_more_link')] ?? '#show-more-btn-block',
                        'class' => 'btn-lg promotions-section__see-more-btn ' . (!isset($data[localize_u('see_more_link')]) ? 'no-link' : ''),
                    ])
                </div>
            </div>
        @endif
    </div>
</section>

<script>
    $(document).ready(function () {
        $('.promotions-section__see-more-btn.no-link').on('click', function (e) {
            if ($('.promotions-section__see-more-btn').hasClass('no-link')) {
                e.preventDefault();
            }

            var $extraItemsBlock = $('.extra-items-block');

            if ($extraItemsBlock.hasClass('non-display')) {
                $extraItemsBlock.removeClass('non-display');
                $extraItemsBlock.slideDown();
            } else {
                $extraItemsBlock.slideUp(function () {
                    $(this).addClass('non-display');
                });
            }
        });
    });
</script>
