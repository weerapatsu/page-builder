@extends('layouts.master')

@push('scripts')
<script src="//cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
@endpush

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
@endpush

@section('content')
    <div class="container" id="app-page-builder">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1>Page builder</h1>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                {{--<a href="{{ route('page-builder::preview', ['id' => $pageId]) }}" target="_blank"--}}
                    {{--class="btn btn-info pull-right">Preview</a>--}}
            </div>
            <div class="col-md-3">
                <page-setting page-id="{{ $pageId }}"></page-setting>
            </div>

            <div class="col-md-9">
                <h2>Module List</h2>

                <module-toolbox
                    page-id="{{ $pageId }}"
                ></module-toolbox>

                <page-module-listing
                    page-id="{{ $pageId }}">
                </page-module-listing>
            </div>
        </div>

        <module-cms-generator></module-cms-generator>
    </div>
@stop
