<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Page Builder Tool</title>

    <link href="https://fonts.googleapis.com/css?family=Kanit:thin,light,regular,medium,semi-bold,bold&amp;subset=thai" rel="stylesheet">

    @stack('styles')

    @stack('header-scripts')
</head>

<body>
@yield('content')

@stack('scripts')
</body>

</html>
