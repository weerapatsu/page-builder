
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');

window.Vue = require('vue');

/**
 * App plugins
 */

import VueNotifications from 'vue-notifications'
import miniToastr from 'mini-toastr'
import VeeValidate from 'vee-validate';

// If using mini-toastr, provide additional configuration
const toastTypes = {
    success: 'success',
    error: 'error',
    info: 'info',
    warn: 'warn'
};

miniToastr.init({types: toastTypes});

// Here we setup messages output to `mini-toastr`
function toast ({title, message, type, timeout, cb}) {
    return miniToastr[type](message, title, timeout, cb)
}

const options = {
    success: toast,
    error: toast,
    info: toast,
    warn: toast
};

// Activate plugin
Vue.use(VueNotifications, options);
Vue.use(VeeValidate);

/**
 * App components
 */

// Utilities
Vue.component('modal', require('./vue-utilities/modal.vue'));
Vue.component('ck-editor', require('./vue-utilities/ck-editor.vue'));

// Main components
Vue.component('page-module-listing', require('./components/page-module-listing.vue'));
Vue.component('module-cms-generator', require('./components/module-cms-generator.vue'));
Vue.component('module-toolbox', require('./components/module-toolbox.vue'));
Vue.component('page-setting', require('./components/page-setting.vue'));

// Sharing events to components
window.pageBuilderEvent = new Vue();

const app = new Vue({
    el: '#app-page-builder'
});
