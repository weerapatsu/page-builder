<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'namespace' => 'PageBuilder',
    'as' => 'page-builder::',
], function () {

    Route::get('/', 'IndexController@index')->name('index');
    Route::get('{id}/preview/{locale?}', 'IndexController@preview')->name('preview');

    Route::group([
        'prefix' => 'modules',
    ], function () {
        Route::get('/', 'ModuleController@index');
    });

    Route::group([
        'prefix' => 'page-modules',
    ], function () {
        Route::get('{id}', 'PageModuleController@show');
        Route::post('/', 'PageModuleController@store');
        Route::delete('{id}', 'PageModuleController@destroy');
        Route::put('{id}/sort', 'PageModuleController@sortModules');
        Route::get('module/{id}/edit', 'PageModuleController@editModule');
        Route::post('module-property/update', 'PageModuleController@updateModuleProperty');
    });
});
